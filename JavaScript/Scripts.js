jQuery(document).ready(function ($) {
    

    //
    //Scripts para manejo del Slider
    //Basado en "Very Simple Slider" por zuraizm, http://codepen.io/zuraizm/pen/vGDHl
    //

    //  Alertas para debuggeo
	/*
    alert("body"+$('body').width());
	alert("main"+ $('#Main').width());
	alert("contenidow"+ $('#Contenido').width());
	alert("contenidow"+ $('#Contenido').height());
	alert("slideboxw"+ $('#slideBox').width());
	alert("slideboxh"+ $('#slideBox').height());
	alert("Maskwidth"+$('#slideMask').width());
    alert("sliderw"+$('#slider').width());
    alert("sliderh"+$('#slider').height());
    alert("ulwidth"+$('#slider ul').width());
    alert("ulh"+$('#slider ul').height());
    alert("slidew"+$('#slider ul li').width());
    alert("slideh"+$('#slider ul li').height());
    */
    
    //
    //Luego de que el CSS genere el tamaño apropiado para #slideBox, este se usa para establecer el tamaño de los demás elementos del Slider
    //

    var slideBoxWidth = $('#slideBox').width();
    var slideBoxHeight = $('#slideBox').height();
    var slideWidth = $('#slider ul li').width();
    var slideMaskWidth = slideBoxWidth - slideWidth;
	var slideCount = $('#slider ul li').length;
	var sliderUlWidth = slideCount * slideWidth;
	var slideTimer = 6000;
    var myInterval = setInterval(moveRight,slideTimer);

   
    $('#slider li').css({width:slideWidth, height: slideBoxHeight});
    $('#slideMask').css({height : slideBoxHeight});
	$('#slider').css({ width: slideWidth, height: slideBoxHeight });
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	$('#slider ul li:last-child').prependTo('#slider ul');
	$('#slider img').css({ width:slideWidth , height:slideBoxHeight });

    //  Alertas para debuggeo
	/*	
	alert("body"+$('body').width());
	alert("main"+ $('#Main').width());
	alert("header "+$('#Header').width());
	alert("menu "+$('#Menu').width());
    alert("MBmenu "+$('div.MBMenu').width());
	alert("Bmenu "+$('div.MBMenu').width());
    alert("slider"+$('#slider').width());
    alert("ulwidth"+$('#slider ul').width());
    alert("slidew"+$('#slider ul li').width());
    alert("slideh"+$('#slider ul li').height());
    */
    

    //
    //Controles para los botones del Slider
    //

    function moveLeft() {
        $('#slider ul').animate(
		{
            left: + slideWidth
        }, 200, function () 
		{
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        }
		);
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };


    $('a.control_prev').click(function () {
        
        clearInterval(myInterval);
        moveLeft();
        myInterval = setInterval(moveRight,slideTimer);
   
        
    });

    $('a.control_next').click(function () {
        clearInterval(myInterval);
        moveRight();
        myInterval = setInterval(moveRight,slideTimer);
    });



}); 